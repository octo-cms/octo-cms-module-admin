<?php

namespace OctoCmsModule\Admin\View\Components;

use Illuminate\View\Component;

/**
 * Class PageContentHandler
 *
 * @package OctoCmsModule\Core\View\Components
 */
class AdminNavbarComponent extends Component
{


    /**
     * @return \Illuminate\View\View|string
     */
    public function render()
    {

        return view()->first(
            [
                'admin.partials.navbar',
                'admin::partials.navbar'
            ],
            [

            ]
        );
    }
}
