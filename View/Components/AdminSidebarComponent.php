<?php

namespace OctoCmsModule\Admin\View\Components;

use Illuminate\Support\Collection;
use Illuminate\View\Component;
use Nwidart\Modules\Facades\Module;
use OctoCmsModule\Core\Services\CacheService;

/**
 * Class PageContentHandler
 *
 * @package OctoCmsModule\Core\View\Components
 */
class AdminSidebarComponent extends Component
{

    public const SIDEBAR_CACHE_TAG = 'sidebar-items';

    public $items = [];

    public function __construct()
    {
        $this->items = $this->getItems();
    }

    /**
     * @return array|mixed
     */
    protected function getItems()
    {
        $items = CacheService::get(self::SIDEBAR_CACHE_TAG, 'items');

        if (!empty($items)) {
            return $items;
        }

        $items = new Collection();

        foreach (Module::allEnabled() as $module) {
            $sidebarConfig = config($module->getLowerName() . '.admin.sidebar');

            if (empty($sidebarConfig)) {
                continue;
            };

            foreach ($sidebarConfig as $config) {
                $config['module'] = $module->getLowerName();
                $items->add($config);
            }
        }

        $items = $items->sortBy('order')->toArray();

        CacheService::set(self::SIDEBAR_CACHE_TAG, 'items', $items);

        return $items;
    }

    /**
     * @return \Illuminate\View\View|string
     */
    public function render()
    {

        return view()->first(
            [
                'admin.partials.sidebar',
                'admin::partials.sidebar'
            ],
            [

            ]
        );
    }
}
