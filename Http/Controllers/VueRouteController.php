<?php

namespace OctoCmsModule\Admin\Http\Controllers;

use App\Http\Controllers\Controller;

/**
 * Class VueRouteController
 *
 * @package OctoCmsModule\Admin\Http\Controllers
 */
class VueRouteController extends Controller
{
    /**
     * @return mixed
     */
    public function login()
    {
        return view()->first(
            [
            'admin.vue-blank-page',
            'admin::vue-blank-page'
            ],
            ['script' => 'admin/login']
        );
    }

    /**
     * @return mixed
     */
    public function loadConfigs()
    {
        return  view()->first(
            [
            'admin.vue-blank-page',
            'admin::vue-blank-page'
            ],
            ['script' => 'admin/load-configs']
        );
    }
}
