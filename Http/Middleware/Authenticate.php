<?php

namespace OctoCmsModule\Admin\Http\Middleware;

use App\Http\Middleware\Authenticate as AuthenticateBase;

/**
 * Class Authenticate
 *
 * @package OctoCmsModule\Core\Http\Middleware
 */
class Authenticate extends AuthenticateBase
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Request $request
     * @return string|null
     */
    protected function redirectTo($request)
    {
        if (! $request->expectsJson()) {
            return route('admin.vue-route.login');
        }
    }
}
