const PictureLang = {
    id: null,
    picture_id: null,
    lang: '',
    alt: '',
    caption: '',
    title: '',
    description: '',
};

export default PictureLang;
