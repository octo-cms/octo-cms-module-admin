const PictureHtml = {
    src: '',
    webp: '',
    alt: '',
    caption: '',
    description: '',
    title: '',
    newImage: {
        src: null,
        height: null,
        width: null
    }
};

export default PictureHtml;
