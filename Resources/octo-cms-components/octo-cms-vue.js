import Vue from 'vue'
import BootstrapVue from 'bootstrap-vue'
import VueLodash from './plugins/lodash.js';
import VueAxios from './plugins/axios.js';
import VueUuid from './plugins/uuid.js';
import VueLangUtils from "./plugins/langUtils";
import VueLocalStorage from "./plugins/localStorage";
import VueRouterDispatch from "./plugins/routerDispatch";
import VueFullLoading from "./plugins/fullLoading"
import VueErrorMessageHandler from "./plugins/errorMessageHandler"
import Moment from '@adminNodeModules/moment';

Vue.use(BootstrapVue);
Vue.use(VueLodash);
Vue.use(VueAxios);
Vue.use(VueUuid);
Vue.use(VueLangUtils);
Vue.use(VueLocalStorage);
Vue.use(VueRouterDispatch);
Vue.use(VueFullLoading);
Vue.use(VueErrorMessageHandler);

Vue.filter('euro', function (value) {
    if (isNaN(value)) {
        return 'N.D.'
    }

    return parseFloat(value).toLocaleString('it-IT', {
        style: 'currency',
        currency: 'EUR',
    });
});

Vue.filter('integer', function (value) {
    if (isNaN(value) || !value) {
        value = 0
    }
    return value.toLocaleString('it-IT', {
        style: 'decimal',
        maximumFractionDigits: 0,
    });
});

Vue.filter('date', function (value) {

    let date = Moment(value, "YYYY-MM-DD");

    return date.isValid() ? date.format('DD/MM/YYYY') : value;
});

export default Vue;
