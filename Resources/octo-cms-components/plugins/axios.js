import axios from '@adminNodeModules/axios';

const VueAxios = {
    install(Vue) {

        if (!Vue.prototype.$api) {
            const axiosInstance = axios.create({
                baseURL: process.env.MIX_BASE_URL,
                headers: {
                    'X-Requested-With': 'XMLHttpRequest'
                },
                withCredentials: true,
            });

            axiosInstance.interceptors.response.use(
                async (config) => {
                    return config
                },
                (err) => {
                    const {config, request: {status}} = err;
                    if(status === 401 && config) {
                       console.log('Unauthorized!!!!')
                    }

                    throw err;

                })

            Object.defineProperty(Vue.prototype, '$api', { value: axiosInstance });
        }
    },
};

export default VueAxios;
