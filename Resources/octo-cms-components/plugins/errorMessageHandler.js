import _ from '@adminNodeModules/lodash';

const VueErrorMessageHandler = {
    install(Vue) {
        // prevent duplicate instance on SSR
        if (!Vue.prototype.$errorMessageHandler) {
            Vue.prototype.$errorMessageHandler = {
                vm: new Vue(),
                display: function (err) {
                    const errorStatus = err.response.data.status;
                    const errorTitle = `${err.response.statusText} (${err.response.status})`;

                    if (_.isObject(errorStatus)) {
                        _.each(errorStatus, error => {
                            this.showToast(error[0], errorTitle);
                        })
                    } else {
                        this.showToast(err.response.data.message, errorTitle);
                    }
                },
                showToast: function(message, title) {
                    this.vm.$bvToast.toast(message, {
                        title: title,
                        variant: 'danger',
                    })
                }
            }
        }
    },
};

export default VueErrorMessageHandler;
