import { v1, v3, v4, v5 } from "@adminNodeModules/uuid";

export const uuid = { v1, v3, v4, v5 };

const VueUuid = {
    install(Vue) {
        // prevent duplicate instance on SSR
        if (!Vue.prototype.$uuid) {
            Object.defineProperty(Vue.prototype, '$uuid', { value: uuid });
        }
    },
};

export default VueUuid;
