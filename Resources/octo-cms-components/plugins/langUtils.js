import _ from '@adminNodeModules/lodash';

const VueLangUtils = {
    install(Vue) {
        // prevent duplicate instance on SSR
        if (!Vue.prototype.$completeLangs) {

            let languages = ['it'];

            let setting=localStorage.getItem('settings');
            if (setting) {
                let ls = JSON.parse(setting);
                languages=ls.languages;
            }

            Vue.prototype.$completeLangs = function (collection, model, id = null, idKey = null) {
                _.each(languages, language => {
                    if (!_.find(collection, (item) => {
                        return item.lang === language;
                    })) {
                        const itemLang = this.$_.cloneDeep(model);

                        itemLang.lang = language;

                        if (id === null || idKey === null) {
                            itemLang[idKey] = id;
                        }

                        collection.push(itemLang);
                    }
                });
            }
        }

        if (!Vue.prototype.$langLabel) {

            Vue.prototype.$langLabel = function (dataLangs, attribute, language) {

                if (!language) {
                    language = 'it';
                }

                const dataLang = _.find(dataLangs, (item) => {
                    return item.lang === language
                });

                return (dataLang && dataLang[attribute]) ? dataLang[attribute] : '';
            }
        }
    },
};

export default VueLangUtils;
