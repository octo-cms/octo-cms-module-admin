const VueLocalStorage = {
    install(Vue) {
        // prevent duplicate instance on SSR
        if (!Vue.prototype.$localStorage) {
            Vue.prototype.$localStorage = {
                set: function (key, value) {
                    localStorage.setItem(key, JSON.stringify(value))
                },
                get: function (key) {
                    const value = localStorage.getItem(key);
                    return value ? JSON.parse(value) : null;
                },
                saveSettings: function (settings) {
                    localStorage.setItem('settings', JSON.stringify(settings))
                },
                getSettings: function () {
                    return this.get('settings');
                },
                getLanguages: function () {
                    return this.getSettings().languages;
                },
                setSettingsByName: function (name, value) {
                    const settings = this.getSettings();
                    settings[name] = value;
                    this.saveSettings(settings);
                }
            }
        }
    },
};

export default VueLocalStorage;
