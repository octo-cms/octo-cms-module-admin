import _ from '@adminNodeModules/lodash';

const VueRouterDispatch = {
    install(Vue) {
        // prevent duplicate instance on SSR
        if (!Vue.prototype.$routerDispatch) {
            Vue.prototype.$routerDispatch = {
                redirect: function (path, queryParams = {}) {
                    let fullPath = process.env.MIX_ADMIN_BASE_URL + '/' + path;
                    if (!_.isEmpty(queryParams)) {
                        _.forEach(queryParams, (query, key) => {
                            fullPath = fullPath.replace('{' + key + '}', query)
                        })
                    }
                    window.location.href = fullPath;
                }
            }
        }
    },
};

export default VueRouterDispatch;
