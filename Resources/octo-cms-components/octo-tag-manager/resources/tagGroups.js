const tagGroups = {
    PRODUCT: 'product',
    SERVICE: 'service',
    BRAND: 'brand',
    SUPPLIER: 'supplier',
    CATEGORY_PRODUCT: 'categoryProduct',
    NEWS: 'news',
    CATEGORY_NEWS: 'categoryNews',
};

export default tagGroups
