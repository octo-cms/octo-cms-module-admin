<div class="main-sidebar bg-dark text-light d-flex flex-column">
    <ul class="list-group list-group-flush w-100">
        @foreach($items as $item)
            @if(!empty($item['childs']))
                <li class="list-group-item active text-uppercase">
                    @lang($item['module'] . '::sidebar.' . $item['label'])
                </li>
                @foreach($item['childs'] as $child)
                    <a class="list-group-item list-group-item-action text-light text-capitalize"
                       href="{{!empty($child['route']) ? route($child['route']) : '#' }}">
                        @lang($item['module'] . '::sidebar.' . $child['label'])
                    </a>
                @endforeach
            @else
                <li class="list-group-item text-capitalize">
                    @lang($item['module'] . '::sidebar.' . $item['label'])
                </li>
            @endif
        @endforeach
    </ul>
    <ul class="list-group list-group-flush w-100">
        <li class="list-group-item active text-uppercase">
            Settings
        </li>
        @foreach($items as $item)
            @if(!empty($item['settings']))
                @foreach($item['settings'] as $setting)
                    <a class="list-group-item list-group-item-action text-light text-capitalize"
                       href="{{!empty($setting['route']) ? route($setting['route']) : '#' }}">
                        @lang($item['module'] . '::sidebar.' . $setting['label'])
                    </a>
                @endforeach
            @endif
        @endforeach
    </ul>
</div>
