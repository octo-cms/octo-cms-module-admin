<!doctype html>
<html lang="{{app()->getLocale()}}">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>{{env('APP_NAME')}}</title>

    <!-- Load required Bootstrap and BootstrapVue CSS -->
    <link type="text/css" rel="stylesheet" href="{{mix('assets-' . env('MIX_ADMIN_PREFIX' ) . '/css/style.min.css')}}"/>

    <!-- Load polyfills to support older browsers -->
    <script src="//polyfill.io/v3/polyfill.min.js?features=es2015%2CIntersectionObserver"
            crossorigin="anonymous"></script>

    @stack('css')

</head>
<body>
<x-admin-navbar></x-admin-navbar>
<x-admin-sidebar></x-admin-sidebar>
<div id="app" class="main-content">
    <page></page>
</div>

<script src="{{mix('assets-' . env('MIX_ADMIN_PREFIX') . '/vue/' . $script . '.min.js' )}}"></script>

</body>
</html>
