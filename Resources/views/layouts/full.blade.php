<!doctype html>
<html lang="{{app()->getLocale()}}">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>{{env('APP_NAME')}}</title>

    <!-- Load required Bootstrap and BootstrapVue CSS -->
    <link type="text/css" rel="stylesheet" href="{{mix('assets-' . env('MIX_ADMIN_PREFIX' ) . '/css/style.min.css')}}"/>

    <!-- Load polyfills to support older browsers -->
    <script src="//polyfill.io/v3/polyfill.min.js?features=es2015%2CIntersectionObserver"
            crossorigin="anonymous"></script>

</head>
<body>
<div class="min-vh-100">
    <x-admin-navbar></x-admin-navbar>
    <div style="margin-left: 200px; margin-top: 50px ">
        @yield('content')
    </div>

    @stack('scripts')
</div>
</body>
</html>
