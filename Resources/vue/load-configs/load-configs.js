import Vue from '@octoCmsComponents/octo-cms-vue'
import PageComponent from './LoadConfigsView';

var app = new Vue({
    el: '#app',
    components: {
        page : PageComponent
    }
});
