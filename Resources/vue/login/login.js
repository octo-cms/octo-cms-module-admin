import Vue from '@octoCmsComponents/octo-cms-vue'
import PageComponent from './LoginView';

var app = new Vue({
    el: '#app',
    components: {
        page : PageComponent
    }
});
