const routes = {
    SANCTUM_CSRF_COOKIE: 'sanctum/csrf-cookie',

    /** USER END POINTS **/
    USER_LOGIN: 'api/admin/v1/login',

    /** CONFIG END POINTS **/
    CONFIG_MODULES_INDEX: 'api/admin/v1/modules',
    CONFIG_SETTINGS_INDEX: 'api/admin/v1/settings',
    CONFIG_SETTINGS_POST: 'api/admin/v1/settings',
    CONFIG_SETTINGS_PUT: 'api/admin/v1/settings',
    CONFIG_SETTINGS_SITE_POST: 'api/admin/v1/sitebuilder/settings',

    /** CONFIG END POINTS **/
    PAGE_LOGIN: '',
    PAGE_LOAD_CONFIGS: 'load-configs',
    PAGE_CORE_INDEX: 'core/registry',

    BLOCK_HTML_UPLOAD_MEDIA: 'api/admin/v1/sitebuilder/block-html/upload-media',
};

export {
    routes
}
