<?php

Route::prefix(env('MIX_ADMIN_PREFIX'))->group(
    function () {

        Route::get(
            '',
            getRouteAction("Admin", "VueRouteController", 'login')
        )
        ->name('admin.vue-route.login');

        Route::group(
            ['middleware' => ['auth-admin']],
            function () {
                Route::get(
                    'load-configs',
                    getRouteAction("Admin", "VueRouteController", 'loadConfigs')
                )
                ->name('admin.vue-route.load-configs');
            }
        );
    }
);
