const dotenvExpand = require('dotenv-expand');
dotenvExpand(require('dotenv').config({ path: '../../.env'/*, debug: true*/}));

const mix = require('laravel-mix');
require('laravel-mix-merge-manifest');

mix.setPublicPath('../../public').mergeManifest();

require('laravel-mix-alias');
mix.alias({
    '@octoCmsComponents': './Resources/octo-cms-components',
    '@adminNodeModules': './node_modules',
});

const assetPath = '../../public/assets-' + process.env.MIX_ADMIN_PREFIX;

mix.sass(__dirname + '/Resources/assets/sass/app.scss', assetPath + '/css/style.min.css');
mix.copy(__dirname + '/Resources/assets/img', assetPath + '/img');

const assetVuePath = assetPath + '/vue/admin/'

mix.js(__dirname + '/Resources/vue/login/login.js', assetVuePath + 'login.min.js');
mix.js(__dirname + '/Resources/vue/load-configs/load-configs.js', assetVuePath + 'load-configs.min.js');

if (mix.inProduction()) {
    mix.version();
}

